var index = body = img = 0, nextimg = new Image(), previmg = new Image()

function zero(num, pad)
	{
	num = "000"+num
	return num.substr(num.length - pad)
	}

function update()
	{
	if (!body) body = document.body
	if (!img)  img  = document.getElementsByTagName('img')[0]
	if (document.location.hash == "")
		document.location.hash = '#1'
	if (document.location.hash == '#last')
		{
		document.location.hash = images.length
		update()
		}
	else
		{
		index = parseInt(document.location.hash.substring(1))-1
		img.src = images[index]
		if (index < images.length-1)
			{
			nextimg.src = images[index+1]
			}
		if (index > 0)
			{
			previmg.src = images[index-1]
			}
		}
	}
function key(event)
	{
	switch (event.keyCode)
		{
		case 81:  // q
		case 37:  // left
		case 188: // ,
			prev()
			break
		case 80:  // p
		case 39:  // right
		case 190: // .
			next()
			break
		default:
			console.log(event.keyCode)
			break
		}
	}
function loaded()
	{
	if (img.width > img.height)
		{
		body.style.overflowX = 'auto'
		body.style.overflowY = 'hidden'
		img.style.width      = 'auto'
		img.style.height     = '100%'
		window.scrollTo(window.innerWidth,0)
		
		// hscroll
		// Mozilla
		if(body.addEventListener) body.addEventListener('DOMMouseScroll', moveObject, false)
		// IE/OPERA
		body.onmousewheel = moveObject
		}
	else
		{
		body.style.overflowX = 'hidden'
		body.style.overflowY = 'auto'
		img.style.width      = '100%'
		img.style.height     = 'auto'
		window.scrollTo(0,0)
		
		// hscroll
		// Mozilla
		if(body.removeEventListener) body.addEventListener('DOMMouseScroll', moveObject, false)
		// IE/OPERA
		body.onmousewheel = null
		}
	img.style.opacity = 1
	}
function next()
	{
	index++
	if (index == images.length)
		{
			document.location.href = "//"+document.location.host+"/"+manga+"/"+zero(parseInt(volume,10)+1, volume.length)+"#1"
		}
	else
		{
		img.style.opacity = 0.3
		document.location.hash = '#'+(index+1)
		update()
		}
	}
function prev()
	{
	index--
	if (index < 0)
		{
			document.location.href = "//"+document.location.host+"/"+manga+"/"+zero(parseInt(volume,10)-1, volume.length)+"#last"
		}
	else
		{
		img.style.opacity = 0.3
		document.location.hash = '#'+(index+1)
		update()
		}
	}
function moveObject(event)
	{
	var delta = 0
	if (!event) event = window.event
	// normalize the delta
	if (event.wheelDelta)
		{
		// IE & Opera
		delta = event.wheelDelta / 120
		}
	else if (event.detail) // W3C
		{
		delta = -event.detail / 3
		}
	function doNothing()
		{
		return false
		}
	body.scrollLeft-=delta*20
	window.addEventListener('DOMMouseScroll', doNothing, true)
	if (event.preventDefault)
		event.preventDefault()
	event.returnValue = false
	}
