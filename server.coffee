#!/usr/bin/env coffee

util = require 'util'
http = require 'http'
net  = require 'net'
url  = require 'url'
fs   = require 'fs'

host = 'stoptazmo.com'

clientjs = fs.readFileSync './client.js', 'utf8'

srv = http.createServer (preq, pres) ->
	util.log "Asked: #{util.inspect preq.url}"
	[manga, volume] = decodeURIComponent(url.parse(preq.url, true).pathname).substring(1).split '/'
	switch preq.url
		when '/'
			pres.writeHead 200, 'Content-Type': 'text/html'
			pres.end """
			         <!doctype html>
			         <html>
			         	<head>
			         		<title>Search</title>
			         		<style>
			         			body {margin: 0; padding: 0;}
			         			#bar {position: fixed; top: 0; left: 0; width: 100%; background: white; box-shadow: 0 0.4em 1em rgba(0,0,0,0.2);}
			         			#text {overflow: hidden;}
			         			#filter-input {box-sizing: border-box; border: none; display: block; width: 100%;}
			         			#filter-options {float: right;}
			         			#filter-input, #filter-options {height: 1.5em;}
			         			hr {border: 1px solid gray; border-bottom-width: 0; margin: 0;}
			         			#results {margin-top: 2em;}
			         			li:nth-child(even) {background: lightgray;}
			         		</style>
			         		<script>
			         			var script = document.createElement("script")
			         			script.src = '/mangas.js'
			         			script.onload = function()
			         				{
			         				var buffer = ""
			         				for (var i = 0, l = mangas.length; i < l; i++)
			         					{
			         					buffer += '<li><a href="'+mangas[i][0]+'">'+mangas[i][1]+'</a></li>'+"\\n"
			         					}
			         				document.getElementById('results').innerHTML = buffer
			         				}
			         		</script>
			         	</head>
			         	<body onload="document.head.appendChild(script);document.getElementById('filter-input').focus()">
			         		<div id="bar">
			         			<select id="filter-options">
			         				<option>Filter</option>
			         				<option>Regex</option>
			         				<optgroup label="Starting with">
			         					<option>0-9</option>
			         					#{("<option>#{letter.toString(36).toUpperCase()}</option>" for letter in [10..35]).join "\n"}
			         				</optgroup>
			         			</select>
			         			<div id="text">
			         				<input type="text" id="filter-input">
			         			</div>
			         			<hr>
			         		</div>
			         		<ul id="results">
			         			<p>Loading manga list...</p>
			         		</ul>
			         	</body>
			         </html>
			         """
		when '/favicon.ico'
			pres.writeHead 200, { 'Content-Type': 'image/x-icon', 'Content-Length': 318 }
			pres.write new Buffer('AAABAAEAEBAQAAAAAAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAA/9QAAAAAAGlpaQD///8AAP+EAAD/uwAA/0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAERESIiIhERERESERERIRERERERERERERERERQBERERERERBgMxEREREREQYzERERERETFRUREhERERMxFhEiEREREzERESIRERETERMSIRERIREREyIRERESERESIREREREhESIRERERERIiIRERERERERERERERERERERERERH4HwAA9+8AAPx/AAD4PwAA8B8AAPAfAADwGwAA8BMAAPATAADwBwAA0AcAAOAXAADgLwAA6F8AAPe/AAD//wAA', 'base64')
		when '/client.js'
			pres.writeHead 200, { 'Content-Type': 'text/javascript'}
			pres.end clientjs
		when '/mangas.js'
			pres.writeHead 200, { 'Content-Type': 'text/javascript'}
			con = http.createClient 80, host
			req = con.request "/online-manga/series-list/entire_list/", {host: host, 'user-agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11"}
			con.on 'error', (error) ->
				util.log error.message
			req.on 'response', (res) ->
				data = ""
				res.on 'data', (chunk) ->
					data += chunk
				res.on 'end', ->
					util.log "Got: #{host}/online-manga/series-list/entire_list/"
					mangas = []
					for line in data.toString().split /<a\ |<\/a>/
						if line.match /^href='http:\/\/stoptazmo\.com\/manga-series\//
							addr = "#{line.split("'")[1].replace 'http://stoptazmo.com/manga-series/', "/"}001"
							name = line.split(/\<|\>/)[3]
							mangas.push [addr, name]
					pres.end "var mangas = #{JSON.stringify mangas}"
			req.end()
			util.log "Getting: #{host}/online-manga/series-list/entire_list/"
		else
			if not volume? or volume is 'undefined' or manga in ['forum', 'img', 'download']
				pres.writeHead 404
				pres.end()
			else
				con = http.createClient 80, host
				req = con.request "/downloads/manga_viewer.php?series=#{manga}&chapter=#{manga}_#{volume}.zip", {host: host, 'user-agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11"}
				con.on 'error', (error) ->
					util.log error.message
				req.on 'response', (res) ->
					data = ""
					res.on 'data', (chunk) ->
						data += chunk
						util.log data.length
					res.on 'end', ->
						util.log "Got: #{host}/downloads/manga_viewer.php?series=#{manga}&chapter=#{manga}_#{volume}.zip"
						images = []
						for i in data.toString().split "'"
							#console.log i
							images.push i.replace /http:\/\/read\.stoptazmo\.com\/resize_img\.php\?url=|\&width=[0-9]+\&height=[0-9]+/g, '' if i.match /^http:\/\/read\.stoptazmo\.com\//
						images.pop()
						pres.writeHead 200, 'Content-Type': 'text/html'
						pres.end """
						         <!doctype html>
						         <html>
						         	<head>
						         		<title>#{manga} - #{volume}</title>
						         		<style>
						         		body {margin: 0; padding: 0; width: 100%; height: 100%;}
						         		img  {-webkit-transition: opacity 0.75s ease-in-out;-moz-transition: opacity 0.75s ease-in-out;}
						         		</style>
						         		<script>var images = #{JSON.stringify images}, manga = '#{manga}', volume = '#{volume}'</script>
						         		<script src="/client.js"></script>
						         	</head>
						         	<body onload="update()" onkeydown="key(event)" onhashchange="update()">
						         		<img onmouseup="next()" onload="loaded()" style="width: 100%; height: auto;">
						         	</body>
						         </html>
						         """
				req.end()
				util.log "Getting: #{host}/downloads/manga_viewer.php?series=#{manga}&chapter=#{manga}_#{volume}.zip"

srv.listen 80, '82.13.153.53', util.log "Server running"
